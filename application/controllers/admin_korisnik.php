<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class admin_korisnik extends MY_frontedncontroller {
    
    public function __construct() {
        parent::__construct(); 
            
        }

	public function index()
	{
            if($uloga="Administrator"){
            $this->load->model('model_admin','admin');
            $this->load->model('model_korisnici','korisnici');
            $uloga=$this->session->userdata('uloga');
            $dugme=$this->input->post('btnIzmena');
            $id=  $this->session->userdata('tmp_id_k');
            if(isset($dugme)){
                $ime=$this->input->post('tbIzmena');
                $this->korisnici->ime=$ime;
                $this->korisnici->idkorisnik=$id;
                echo 'Izmena';
                $this->korisnici->izmeniIme();
                $this->session->unset_userdata('tmp_id_k');
                redirect('admin_korisnik/clanovi');
            }
            }
            
            

        }
    
        public function clanovi($opcija=null,$id=null){
            $formm='admin_korisnik';
            $this->load->model('model_admin','admin');
            $this->load->model('model_korisnici','korisnici');
            $podaci=array();
            $text="";
            $uloga=$this->session->userdata('uloga');
            if($uloga="Administrator"){
                $podaci['korisnici']=$this->admin->dohvatiKorisnike();
            }
            if($opcija=="obrisi" && $id!=null && $uloga="Administrator"){
                $this->korisnici->idkorisnik=$id;
                $podaci['korisnik']=$this->korisnici->obrisiKorisnika();
                redirect('admin_korisnik/clanovi');
            }
            if($opcija=="izmeni" && $id!=null && $uloga="Administrator"){
                $this->korisnici->idkorisnik=$id;
                $podaci['korisnik']=$this->korisnici->korisnik();
                $text=$podaci['korisnik']['ime_korisnik'];
                $this->session->set_userdata('tmp_id_k',$id);
                //redirect('admin_korisnik/clanovi');
            }
            $btnSub=array(
                'name'=>'btnIzmena',
                'class'=>'btn btn-alt waves-button waves-effect waves-light',
                'type'=>'submit',
                'content'=>'Izmeni'
            );
            $lbIzmena=array(
                'class'=>'floating-label',
                'name'=>'lbIzmena',
                'id'=>'lbIzmena'
            );
            $tbIzmena=array(
                'class'=>'form-control textarea-autosize',
                'id'=>'float-textarea-autosize',
                'rows'=>'1',
                'type'=>'text',
                'name'=>'tbIzmena',
                'value'=>$text
            );
            $podaci['lbIzmen']=$lbIzmena;
            $podaci['taIzmena']=$tbIzmena;
            $podaci['btnIzmena']=$btnSub;
            $podaci['formm']=$formm;
            $this->load_view('content_adminP',$podaci);
        }
        public function galerije($opcija=null,$id=null){
            $formm='admin_korisnik/galerije/';
            $this->load->model('model_admin','admin');
            //$this->load->model('model_postovi','post');
            $podaci=array();
            $uloga=$this->session->userdata('uloga');
            if($uloga="Administrator"){
                $podaci['galerije']=$this->admin->dohvatiGalerije();
            }
           
            if($opcija=="obrisi" && $id!=null){
                $this->admin->idGal=$id;
                $this->admin->obrisiGaleriju();
                redirect('admin_korisnik/galerije/');
            }
            $this->load_view('content_adminP',$podaci);
        }        
        public function log($opcija=null,$id=null){
            $formm='admin_korisnik/log/';
            $this->load->model('model_admin','admin');
            //$this->load->model('model_postovi','post');
            $podaci=array();
            $uloga=$this->session->userdata('uloga');
            if($uloga="Administrator"){
                $podaci['logs']=$this->admin->dohvatiLog();
            }

            if($opcija=="obrisi" && $id!=null){
                $this->admin->idLog=$id;
                $this->admin->obrisiLog();
                redirect('admin_korisnik/log/');
            }
            $this->load_view('content_adminP',$podaci);
        }        
}    