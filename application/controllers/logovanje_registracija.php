<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Logovanje_registracija extends MY_frontedncontroller{
    public function __construct() {
        parent::__construct();    
    }
        
    public function index(){
        $this->load->helper('form');
        $korisnik=$this->session->userdata('korisnik');
        if(!empty($korisnik)){ redirect('pocetna');}        
        $podaci=array();
        $forma=array(
            'id'=>'formaReg',
            'name'=>'formaRegistracija',
            'method'=>'POST'
        );
        $regIme=array(
            'id'=>'float-text',
            'name'=>'tbIme',
            'class'=>'form-control',
            'type'=>'text'
        );
        $regPrez=array(
            'id'=>'float-text',
            'name'=>'tbPrezime',
            'class'=>'form-control',
            'type'=>'text'
        );
        $regEmail=array(
            'id'=>'float-text',
            'name'=>'tbEmail',
            'class'=>'form-control',
            'type'=>'text'
        );
        $regUsername=array(
            'id'=>'float-text',
            'name'=>'tbUsername',
            'class'=>'form-control',
            'type'=>'text'
        );        
        $regPass=array(
            'id'=>'float-text',
            'name'=>'tbPass',
            'class'=>'form-control',
            'type'=>'password'
        );
        $regCPass=array(
            'id'=>'float-text',
            'name'=>'tbCPass',
            'class'=>'form-control',
            'type'=>'password'
        );
        $regRodjenja=array(
            'id'=>'input-date',                
            'name'=>'pcDate',
            'class'=>'form-control',
            'type'=>'date'
        ); 
        $btnRegistracija=array(             
            'name'=>'btnRegistracija',
            'class'=>'btn btn-blue waves-button waves-light waves-effect',
            'type'=>'submit',
            'content'=>'Registracija'
        );
        $btnReset=array(               
            'name'=>'btnPonisti',
            'class'=>'btn waves-button waves-effect',
            'content'=>'Ponisti',
            'type'=>'button'
        );

            $podaci['Form']=$forma;
            $podaci['Ime']=$regIme;
            $podaci['Prezime']=$regPrez;
            $podaci['Email']=$regEmail;
            $podaci['Username']=$regUsername;            
            $podaci['Sifra']=$regPass;
            $podaci['CSifra']=$regCPass;
            $podaci['Rodjenje']=$regRodjenja;
            $podaci['Registracija']=$btnRegistracija;
            $podaci['Ponisti']=$btnReset;
        $dugme=$this->input->post('btnRegistracija');
        if(isset($dugme)){
            $this->load->library('form_validation');
            $this->form_validation->set_rules('tbIme','Ime','required|xss_clean|callback_imeprezime');
            $this->form_validation->set_rules('tbPrezime','Prezime','required|xss_clean|callback_imeprezime');
            $this->form_validation->set_rules('tbEmail','E-mail','required|xss_clean|valid_email');
            $this->form_validation->set_rules('tbPass','Lozinka','required|min_length[5]|max_length[15]');
            $this->form_validation->set_rules('tbCPass','Ponovite lozinku','required|matches[tbPass]');
            $this->form_validation->set_message('min_length','Morate uneti najmanje 5 karaktera u polje %s');
            $this->form_validation->set_message('max_length','Mozete uneti najvise 15 karaktera u polje %s');
            $this->form_validation->set_message('valid_email','Niste ispravno uneli e-mail!');
            $this->form_validation->set_message('matches','Polje %s se ne poklapa sa unetom lozinkom!');
            $this->form_validation->set_message('required','Polje %s je obavezno!');
            $this->form_validation->set_message('imeprezime','Polje %s niste dobro uneli! Pocetno slovo mora biti veliko! Min. dužina je 2 karaktera, a max 21!');
            if($this->form_validation->run()){
             $this->load->model('model_korisnici','korisnik');
             $email=$this->input->post('tbEmail');
             $this->korisnik->email=$email;
             $broj=$this->korisnik->registracija();
             if($broj>0){
                 $this->session->set_flashdata('postoji','Postoji registrovani korisnik sa unetim e-mailom!');
                 redirect('logovanje_registracija','refresh');
             }else{
                $ime=$this->input->post('tbIme');
                $prezime=$this->input->post('tbPrezime');
                $email=$this->input->post('tbEmail');
                $username=  $this->input->post('tbUsername');
                $pass=$this->input->post('tbPass');
                $datumRodj=  $this->input->post('pcDate');
                $this->korisnik->ime=$ime;
                $this->korisnik->prezime=$prezime;
                $this->korisnik->email=$email;
                $this->korisnik->user=$username;
                $this->korisnik->password=$pass;
                $this->korisnik->datumRodjenja=$datumRodj;
                $this->korisnik->uloga=1;
                $this->korisnik->aktivan=1;
                $this->korisnik->unesi();
                $this->session->set_flashdata('uspeh','Uspesno ste se registrovali! Mozete se sada ulogovati!');
                redirect('logovanje_registracija','refresh');
              }
            }
        }                

        $this->load_view('content_registar',  $podaci);

    }

    public function login(){
        $korisnik2= $this->session->userdata('korisnik');
        echo $korisnik2;        
        $this->load->library('form_validation');
        $this->load->helper('form');
        $podaci=array();
        $forma=array(
            'id'=>'formaReg',
            'name'=>'formaRegistracija',
            'method'=>'POST'
        );
        $logEmail=array(
            'id'=>'float-text',
            'name'=>'tbEmail',
            'class'=>'form-control',
            'type'=>'text'
        );
        $logPass=array(
            'id'=>'float-text',
            'name'=>'tbPass',
            'class'=>'form-control',
            'type'=>'password'
        );
        $btnLogovanje=array(             
            'name'=>'btnLogovanje',
            'class'=>'btn btn-blue waves-button waves-light waves-effect',
            'type'=>'submit',
            'content'=>'Logovanje'
        );
        $btnReset=array(               
            'name'=>'btnPonisti',
            'class'=>'btn waves-button waves-effect',
            'content'=>'Ponisti',
            'type'=>'button'
        );

        $podaci['Email']=$logEmail;
        $podaci['Sifra']=$logPass;
        $podaci['Registracija']=$btnLogovanje;
        $podaci['Ponisti']=$btnReset;
        //print"cao";
        
        $dugme=$this->input->post('btnLogovanje');
        if(isset($dugme)){
            $email=$this->input->post('tbEmail');
            $lozinka=md5($this->input->post('tbPass'));
            $this->load->library('form_validation');
            $this->form_validation->set_rules('tbEmail','E-mail','required|trim|valid_email');
            $this->form_validation->set_rules('tbPass','Lozinka','required|min_length[5]');
            $this->form_validation->set_message('required','Polje <b>%s</b> je obavezno!');
            $this->form_validation->set_message('min_length','Minimalna duzina polja <b>%s</b> je 5!');
            $this->form_validation->set_message('valid_email','Unesite polje <b>%s</b> u ispravnom formatu!');
            if($this->form_validation->run()){
                $this->load->model('model_korisnici','korisnik');
                $this->korisnik->email=$email;
                $this->korisnik->password=$lozinka;
                $podaci['korisnik']=$this->korisnik->dohvati();
                if(!empty($podaci['korisnik'])){
                    $korisnik_sesija=$podaci['korisnik']['id_korisnik'];
                    $this->session->set_userdata('id_korisnik',$korisnik_sesija);
                    $uloga=$podaci['korisnik']['naziv_uloga'];
                    $this->session->set_userdata('uloga',$uloga);
                    $ime=$podaci['korisnik']['ime_korisnik'];
                    $this->session->set_userdata('ime',$ime);
                    $prez=$podaci['korisnik']['prezime_korisnik'];
                    $this->session->set_userdata('prezime',$prez);                    
                    $this->session->set_flashdata('ulogovao', 'Uspesno ste se ulogovali <b>'.$ime."</b> !");
                    if($podaci['korisnik']['id_uloga']=='1'){
                        redirect('pocetna_korisnik');                       
                    }else if($podaci['korisnik']['id_uloga']=='2'){
                        redirect('pocetna_korisnik');                       
                    }else{
                        redirect('registracija');
                    }

                }
            }else{ 
                $this->session->set_flashdata('validacija',  validation_errors());
            }
        }else{
            //redirect('logovanje_registracija/login','refresh');
        }        

        $this->load_view('content_login',$podaci);               
    }

    public function logout(){
            $korisnik= $this->session->userdata('id_korisnik');
            if(!empty($korisnik)){
                $this->session->unset_userdata('id_korisnik');
                $this->session->sess_destroy();
                redirect('logovanje_registracija');
            }
    }
    //za proveru imena i prezimena, prvo veliko slovo
    public function imeprezime($uneto){
        $reg="/^[A-Z][a-z]{2,20}$/";
        if(!preg_match($reg, $uneto)){
            return false;
        }
        else {
            return true;
        }
    }
}
