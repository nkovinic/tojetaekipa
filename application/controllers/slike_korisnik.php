<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class slike_korisnik extends MY_frontedncontroller {
    public function __construct() {
        parent::__construct();    
    }
    
    public function index(){
        $podaci=array();
        $this->load->library('pagination');
        $this->load->helper('form');
        $form1='slike_korisnik/dodajGaleriju';
        $form2='slike_korisnik/dodajSliku';
        $btnSub1=array(
            'name'=>'btnGalerija',
            'class'=>'btn btn-alt waves-button waves-effect waves-light',
            'type'=>'submit',
            'content'=>'Dodaj novu galeriju'
        );
        $lbGale1=array(
            'class'=>'floating-label',
            'name'=>'lbGalerija',
            'id'=>'lbGalerija'
        );
        $tbGale1=array(
            'class'=>'form-control textarea-autosize',
            'id'=>'float-textarea-autosize',
            'rows'=>'1',
            'type'=>'text',
            'name'=>'tbGalerija',
        );
        $btnSub2=array(
            'name'=>'btnSlika',
            'class'=>'btn btn-alt waves-button waves-effect waves-light',
            'type'=>'submit',
            'content'=>'Dodaj novu Sliku'
        );
        $lbSlika=array(
            'class'=>'floating-label',
            'for'=>'float-select',
            'name'=>'lbSlika',
            'id'=>'lbSlika'
        );
        $fiSlika=array(
            'class'=>'',
            'id'=>'',
            'name'=>'uploadProfile'
        );
        $ddrGalerija=array(
            'class'=>'form-control',
            'id'=>'float-select',
            'name'=>'inSlika',
        );          
        $podaci['form1']=$form1;
        $podaci['lbGale1']=$lbGale1;
        $podaci['tbGale1']=$tbGale1;
        $podaci['btnGale']=$btnSub1;

        $podaci['form2']=$form2;
        $podaci['lbSlika']=$lbSlika;
        $podaci['fiSlika']=$fiSlika;
        $podaci['btnSlike']=$btnSub2;
        $podaci['ddrGal']=$ddrGalerija;
        $idKor=  $this->session->userdata('id_korisnik');
        $this->load->model('model_Sli_Gal','galS');
        $this->galS->idKor=$idKor;
        $podaci['opcijeGal']=  $this->galS->dohvatiGalerije();

        $this->load_view('content_dodajSliku',$podaci);       
    }
    
    public function dodajGaleriju(){
        $dugme=  $this->input->post('btnGalerija');
        if(isset($dugme)){
            $nazivGal=  $this->input->post('tbGalerija');
            $idKor=  $this->session->userdata('id_korisnik');
            $this->load->model('model_Sli_Gal','galS');
            $this->galS->idKor=$idKor;
            $this->galS->nazivGal=$nazivGal;
            $this->galS->dodajGaleriju();
            redirect('slike_korisnik');
        }
    }
    public function dodajSliku(){
        $podaci=  array();
        $gal=  $this->input->post('opcijaGalerija');
        $idKor=  $this->session->userdata('id_korisnik');
        $dugmeS=  $this->input->post('btnSlike');
        if(isset($dugmeS)){   
          $this->load->helper('file');
          $config['upload_path'] = './img/';
          $config['allowed_types'] = 'gif|jpg|png|JPEG';
          $vreme=$idKor.'_'.time();
          $config['file_name']=$vreme;
          $this->load->library('upload',$config);
          if(!$this->upload->do_upload('uploadProfile')){
               $this->session->set_flashdata('neuspeh',$this->upload->display_errors());
              redirect('slike_korisnik');
          }
          else{
              $data=array('upload_data'=>$this->upload->data());
              $this->resize($data['upload_data']['full_path'], $data['upload_data']['file_name'], $data['upload_data']['image_width'],$data['upload_data']['image_height']);
              $this->load->model('model_Sli_Gal','slika');
              $this->slika->nazivSlike=$data['upload_data']['file_name'];
              $this->slika->malaSlika=$data['upload_data']['file_name'];
              $this->slika->idGal=$gal;
              $this->slika->idKor=$idKor;
              $this->slika->dodajSliku();
              $this->session->set_flashdata('izmenjeno',"Uspesno ste dodali sliku!");
              redirect('slike_korisnik');
          }
        }
    }
    
    function resize($path,$file,$sirina,$visina){
    $config['image_library']="gd2";
    $config['source_image']=$path;
    $config['height']=200;
    $odnos=$visina/200;
    $config['width']=$visina*$odnos;
    $config['new_image']="./img/"."mala_".$file;
    $this->load->library('image_lib',$config);
    $this->image_lib->resize();
}
    
}