<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pocetna_korisnik extends MY_frontedncontroller {
    public function __construct() {
        parent::__construct();    
    }

    public function index(){

        /*if($uloga_provera=="Administrator"){
            redirect("pocetna_admin");
        }*/
        $podaci=array();
        $this->load->library('pagination');
        $this->load->helper('form');
        $form='pocetna_korisnik';
        $btnSub=array(
            'name'=>'btnPost',
            'class'=>'btn btn-alt waves-button waves-effect waves-light',
            'type'=>'submit',
            'content'=>'Postavi post'
        );
        $lbPost=array(
            'class'=>'floating-label',
            'name'=>'lbPost',
            'id'=>'lbPast'
        );
        $taPost=array(
            'class'=>'form-control textarea-autosize',
            'id'=>'float-textarea-autosize',
            'rows'=>'1',
            'type'=>'textarea',
            'name'=>'taPost',
        );
        $podaci['formm']=$form;
        $podaci['lbPost']=$lbPost;
        $podaci['taPost']=$taPost;
        $podaci['btnPost']=$btnSub;
        $podaci['ime_kor']=$this->session->userdata('ime');
        $podaci['prez_kor']=$this->session->userdata('prezime'); 
        $dugme=$this->input->post('btnPost');   
        $idPostS=  $this->session->userdata('idPostS'); 
        if(isset($dugme)){
            $tekstPost=  $this->input->post('taPost');
            if($idPostS=="" || $idPostS==null){
            if($tekstPost!=""){
                $this->load->model('model_postovi','post');
                $idKor=  $this->session->userdata('id_korisnik');
                $imeKor=  $this->session->userdata('ime');
                $prezKor=  $this->session->userdata('prezime');
                $this->post->idKorisnik=$idKor;
                $this->post->teksPost=$tekstPost;
                $this->post->imeKor=$imeKor;
                $this->post->prezKor=$prezKor;
                $this->post->dodaj_post();
            }}

        }
        $dugme2=$this->input->post('btnIzmeni');

        echo $idPostS;
        if(isset($dugme2)){
            $tekstPost2=  $this->input->post('taPost');
            if($tekstPost2!=""){
                $this->load->model('model_postovi','post');
                $idKor=  $this->session->userdata('id_korisnik');
                $imeKor=  $this->session->userdata('ime');
                $prezKor=  $this->session->userdata('prezime');
                $this->post->idPost=$idPostS;
                $this->post->idKorisnik=$idKor;
                $this->post->teksPost=$tekstPost2;
                $this->post->imeKor=$imeKor;
                $this->post->prezKor=$prezKor;
                $this->post->izmeni_post();
                $this->session->unset_userdata('idPostS');
            }
        }        
        //paginacija postovas
        $limit=7;
        $this->load->model('model_postovi','post');
        $idKor=$this->session->userdata('id_korisnik');
        $this->post->idKorisnik=$idKor;            
        $podaci['postovi']=$this->post->pagination_post(7);
        $brojpostova=$this->post->brojpostova();

        $config['total_rows'] = $brojpostova;
        $config['per_page'] = 7;
        $config['uri_segment']= 3;
        $config['base_url'] = base_url()."pocetna_korisnik/index/";
        $config['full_tag_open'] = "<nav align='center'><ul class='pagination'>";
        $config['full_tag_close'] ="</nav></ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";

        $this->pagination->initialize($config); 
        $podaci['pagination_linkovi']=$this->pagination->create_links();          

        $this->load_view('content_postovi',$podaci);
    }
    
    public function download($fajl=null){
        
            $this->load->helper('download');
            $this->load->helper('url');
            $podaci=  file_get_contents(base_url()."/uploads/Dokumentacija.pdf");
            $ime_fajla="Dokumentacija.pdf";
            force_download($ime_fajla,$podaci);
             redirect("pocetna_korisnik");
            
    }

    public function obrisi($idPost=null){
        $this->load->model('model_postovi','post');
        $this->post->idPost=$idPost;
        $this->post->obrisi_post();
        redirect('pocetna_korisnik');

    }
    public function izmeni($idPost=null){
        $podaci=array();
        $this->load->model('model_postovi','post');
        $this->load->library('pagination');
        $this->load->helper('form');
        $this->post->idPost=$idPost;
        $dugme=$this->input->post('btnIzmeni');            
        $podaci['menjanje']=$this->post->dohvati_post();
        $text1=$podaci['menjanje']['text_post'];
        $form='pocetna_korisnik';
        $this->session->set_userdata('idPostS',$idPost);
        $btnSub=array(
            'name'=>'btnIzmeni',
            'class'=>'btn btn-alt waves-button waves-effect waves-light',
            'type'=>'submit',
            'content'=>'Izmeni post'
        );
        $lbPost=array(
            'class'=>'floating-label',
            'name'=>'lbPost',
            'id'=>'lbPast'
        );
        $taPost=array(
            'class'=>'form-control textarea-autosize',
            'id'=>'float-textarea-autosize',
            'rows'=>'1',
            'type'=>'textarea',
            'name'=>'taPost',
            'value'=>$text1
        );
        $podaci['formm']=$form;
        $podaci['lbPost']=$lbPost;
        $podaci['taPost']=$taPost;
        $podaci['btnPost']=$btnSub;
        $podaci['ime_kor']=$this->session->userdata('ime');
        $podaci['prez_kor']=$this->session->userdata('prezime');            

        //paginacija postovas
        $limit=7;
        $this->load->model('model_postovi','post');
        $idKor=$this->session->userdata('id_korisnik');
        $this->post->idKorisnik=$idKor;            
        $podaci['postovi']=$this->post->pagination_post($limit);
        $brojpostova=$this->post->brojpostova();

        $config['total_rows'] = $brojpostova;
        $config['per_page'] = $limit;
        $config['uri_segment']= 3;
        $config['base_url'] = base_url()."pocetna_korisnik/index/";
        $config['full_tag_open'] = "<nav align='center'><ul class='pagination'>";
        $config['full_tag_close'] ="</nav></ul>";
        $config['num_tag_open'] = '<li>';
        $config['num_tag_close'] = '</li>';
        $config['cur_tag_open'] = "<li class='disabled'><li class='active'><a href='#'>";
        $config['cur_tag_close'] = "<span class='sr-only'></span></a></li>";
        $config['next_tag_open'] = "<li>";
        $config['next_tagl_close'] = "</li>";
        $config['prev_tag_open'] = "<li>";
        $config['prev_tagl_close'] = "</li>";
        $config['first_tag_open'] = "<li>";
        $config['first_tagl_close'] = "</li>";
        $config['last_tag_open'] = "<li>";
        $config['last_tagl_close'] = "</li>";

        $this->pagination->initialize($config); 
        $podaci['pagination_linkovi']=$this->pagination->create_links();          

        $this->load_view('content_postovi',$podaci);

    }        

}