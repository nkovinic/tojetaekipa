<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class pretraga_korisnik extends MY_frontedncontroller{
    public function __construct()
    {
        parent::__construct();
    }
    public function index(){
        $form='pocetna_korisnik';
        $btnSub=array(
            'name'=>'btnPretraga',
            'class'=>'btn btn-alt waves-button waves-effect waves-light',
            'type'=>'submit',
            'content'=>'Pretrazi'
        );
        $lbPretraga=array(
            'class'=>'floating-label',
            'name'=>'lbPretraga',
            'id'=>'lbPretraga'
        );
        $tbPretraga=array(
            'class'=>'form-control textarea-autosize',
            'id'=>'float-textarea-autosize',
            'rows'=>'1',
            'type'=>'text',
            'name'=>'tbPretraga',
        );
        $podaci['formm']=$form;
        $podaci['lbPretraga']=$lbPretraga;
        $podaci['tbPretraga']=$tbPretraga;
        $podaci['btnPretraga']=$btnSub;
        
        $this->load_view('content_pretraga',$podaci);
    }

    public function pretraga()
    {
            $this->load->model('model_korisnici','korisnici');
            $rezultat=$this->korisnici->prikazi();
            print json_encode($rezultat);
    }
}
