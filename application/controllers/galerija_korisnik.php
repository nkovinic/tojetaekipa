<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Galerija_korisnik  extends MY_frontedncontroller{
    public function __construct() {
        parent::__construct();    
    }
    public function index(){
        $podaci=array();
        $this->load->library('pagination');
        $this->load->helper('form');
        $form2='galerija_korisnik';
        $btnSub2=array(
            'name'=>'btnSlika',
            'class'=>'btn btn-alt waves-button waves-effect waves-light',
            'type'=>'submit',
            'content'=>'Prikaži slike'
        );
        $lbSlika=array(
            'class'=>'floating-label',
            'for'=>'float-select',
            'name'=>'lbSlika',
            'id'=>'lbSlika'
        );
        $ddrGalerija=array(
            'class'=>'form-control',
            'id'=>'float-select',
            'name'=>'inSlika',
        );
        
        $podaci['form2']=$form2;
        $podaci['lbSlika']=$lbSlika;
        $podaci['btnSlike']=$btnSub2;
        $podaci['ddrGal']=$ddrGalerija;
        $idKor=  $this->session->userdata('id_korisnik');
        $this->load->model('model_Sli_Gal','galS');
        $this->galS->idKor=$idKor;
        $podaci['opcijeGal']=  $this->galS->dohvatiGalerije();
        $dugme=$this->input->post('btnSlike');
        if(isset($dugme)){
            $gal=  $this->input->post('opcijaGalerija');
            $this->load->model('model_Sli_Gal','galS');  
            $this->galS->idKor=$idKor;
            $this->galS->idGal=$gal;
            $podaci['slike_gal']=$this->galS->dohvatiSlike();
        }else{
            print "dugme nije pritisnuto";
        }
        $this->load_view('content_galerija',$podaci);       
    }

    
    
}
