
<div class="content">
    <div class="content-heading">
        <div class="container container-full">
            <h1 class="heading">Pretraga <small></small></h1>
        </div>
    </div>
    <div class="content-inner">
        <div class="container container-full">
            <div class="row row-fix">
                <div class="col-md-3 content-fix">
                    <div class="content-fix-scroll">
                        <div class="content-fix-wrap">
                            <div class="content-fix-inner">


                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <fieldset>
                        <div class="form-group form-group-label">
                            <div class="row">
                                <div class="col-lg-6 col-md-12 col-sm-8">
                                    <form class="navbar-form" align="center">
                                        <label class="floating-label" for="float-text">Pretraga korisnika</label>
                                        <input type="text" name="pretraga" class="form-control" id="pretraga" placeholder="Pretraga po imenu"/>
                                        <br/>
                                        <input type="button" name="btnPretraga" class="btn btn-success" id="btnPretraga" value="Pretraži"/>
                                    </form>
                                    <div class="col-lg-6 col-md-12 col-sm-8 imePretraga" id="imePretraga">
                                        
                                    </div>
                                </div>
                            </div>
			</div>
                    </fieldset>
                </div>
                <div class="col-md-3 content-fix">
                        <div class="content-fix-scroll">
                                <div class="content-fix-wrap">
                                        <div class="content-fix-inner">
                                            

                                        </div>
                                </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>