<nav class="menu menu-right" id="profile">
    <div class="menu-scroll">
        <div class="menu-wrap">
            <div class="menu-top">
                <div class="menu-top-img">
                    <img alt="Ime Prezime" src="<?php echo base_url();?>images/samples/landscape.jpg">
		</div>
		<div class="menu-top-info">
                    <a class="menu-top-user" href="javascript:void(0)"><span class="avatar pull-left"><img alt="alt text for John Smith avatar" src="<?php echo base_url();?>images/users/avatar-001.jpg"></span>
                        <?php
                            $ime=$this->session->userdata('ime');
                            $prezime=$this->session->userdata('prezime');
                            echo $ime." ".$prezime;
                        ?></a>
		</div>
		<div class="menu-top-info-sub">
                    <small><?php
                        $uloga=$this->session->userdata('uloga');
                        echo $uloga;?>
                    </small>
		</div>
            </div>
            <div class="menu-content">
                <ul class="nav">
                    <li>
                        <a href="<?php echo base_url();?>logovanje_registracija/logout"><span class="icon icon-exit-to-app"></span>Odjavi se</a>
                    </li>
		</ul>
            </div>
	</div>
    </div>
</nav>