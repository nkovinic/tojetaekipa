
<div class="content">
    <div class="content-heading">
            <div class="container container-full">
                    <h1 class="heading">Admin panel <small></small></h1>
            </div>
    </div>
    <div class="content-inner">
        <div class="container container-full">
            <div class="row row-fix">
                <div class="col-md-3 content-fix">
                    <div class="content-fix-scroll">
                        <div class="content-fix-wrap">
                            <div class="content-fix-inner">


                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <fieldset>
                        <div class="form-group form-group-label">
                            <div class="row">
                                <div class="col-lg-6 col-md-12 col-sm-8">
                                    <?php
                                    if(isset($korisnici) && is_array($korisnici)){
                                        print form_open($formm);
                                        print form_label('Za izmenu','float-textarea-autosize',$lbIzmen);
                                        print form_input($taIzmena);
                                        print "<br/>";
                                        print form_button($btnIzmena);
                                        print form_close();}
                                    ?>
                                </div>
                            </div>
                        </div>
                    </fieldset>
                    
                    <h2 class="content-sub-heading">Admin panel</h2>
                    <div class="table-responsive">
                        <table class="table table-hover table-stripe" title="Tabela članova">
                            <thead>
                                <tr>
                                        <th>Ime(naziv galerije)</th>
                                        <th>Prezime(vlasnik galerije)</th>
                                        <th>Opcije</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                    if(isset($korisnici) && is_array($korisnici)){
                                     foreach($korisnici as $korisnik){
                                      print '<tr>';
                                        print '<td>'.$korisnik['ime_korisnik'].'</td>';
                                        print '<td>'.$korisnik['prezime_korisnik'].'</td>';
                                        print '<td>';
                                            print anchor('admin_korisnik/clanovi/izmeni/'.$korisnik['id_korisnik'],'Izmeni');
                                            print '&nbsp&nbsp';
                                            print anchor('admin_korisnik/clanovi/obrisi/'.$korisnik['id_korisnik'],'Obrisi');
                                        print '</td>';
                                      print '</tr>';
                                     }
                                    }
                                    if(isset($galerije) && is_array($galerije)){
                                     foreach($galerije as $galerija){
                                      print '<tr>';
                                        print '<td>'.$galerija['naziv_galerija'].'</td>';
                                        print '<td>'.$galerija['ime_korisnik'].'</td>';
                                        print '<td>';
                                           // print anchor('admin_korisnik/clanovi/izmeni/'.$galerija['id_korisnik'],'Izmeni');
                                            print '&nbsp&nbsp';
                                            print anchor('admin_korisnik/galerije/obrisi/'.$galerija['id_galerija'],'Obrisi');
                                        print '</td>';
                                      print '</tr>';
                                     }
                                    }
                                    if(isset($logs) && is_array($logs)){
                                     foreach($logs as $log){
                                      print '<tr>';
                                        print '<td>'.$log['id_log'].'</td>';
                                        print '<td>'.$log['ime_korisnik'].'</td>';
                                        print '<td>';
                                            //print anchor('admin_korisnik/clanovi/izmeni/'.$log['id_korisnik'],'Izmeni');
                                            print '&nbsp&nbsp';
                                            print anchor('admin_korisnik/log/obrisi/'.$log['id_log'],'Obrisi');
                                        print '</td>';
                                      print '</tr>';
                                     }
                                    }
                                ?>
                            </tbody>
                        </table>
                    </div>
                    

                </div>
                <div class="col-md-3 content-fix">
                        <div class="content-fix-scroll">
                                <div class="content-fix-wrap">
                                        <div class="content-fix-inner">
                                            <?php //echo $pagination_linkovi; ?>

                                        </div>
                                </div>
                        </div>
                </div>
            </div>
        </div>
    </div>
</div>