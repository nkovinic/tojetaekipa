<?php echo doctype(); ?>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="IE=edge" http-equiv="X-UA-Compatible">
    <meta content="initial-scale=1.0, width=device-width" name="viewport">
    <title>Material</title>
 <script src="<?php echo base_url();?>/js/fancybox/lib/jquery-1.10.1.min.js" type="text/javascript"></script>   	
<script src="<?php echo base_url();?>/js/fancybox/source/jquery.fancybox.js" type="text/javascript"></script>
<link href="<?php echo base_url();?>/js/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css"/> 
    <!-- css -->
    <link href="<?php echo base_url();?>css/base.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>css/base.css" rel="stylesheet">
    <link href="<?php echo base_url();?>css/style.css" rel="stylesheet">

    <!-- favicon -->
    <!-- ... -->
    <!-- ie -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>
<body class="avoid-fout">