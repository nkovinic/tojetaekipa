<div class="content">
		<div class="content-heading">
			<div class="container container-full">
				<h1 class="heading">Postovi <small></small></h1>
			</div>
		</div>
		<div class="content-inner">
			<div class="container container-full">
				<div class="row row-fix">
					<div class="col-md-3 content-fix">
						<div class="content-fix-scroll">
							<div class="content-fix-wrap">
								<div class="content-fix-inner">
                                                                    
                                                                    
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
                                            <fieldset>
                                                <div class="form-group form-group-label">
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-12 col-sm-8">
                                                            <?php
                                                                print form_open($formm);
                                                                print form_label('Tekst posta','float-textarea-autosize',$lbPost);
                                                                print form_input($taPost);
                                                                print "<br/>";
                                                                print form_button($btnPost);
                                                                print form_close();
                                                            ?>
                                                        </div>
                                                    </div>
						</div>
                                            </fieldset>
                                            <?php
                                                if(isset($postovi) && is_array($postovi)){
                                                    $ime=$ime_kor;
                                                    $prez=$prez_kor;
                                                    foreach($postovi as $post){     
                                            
                                                        echo '<div class="col-lg-3 col-md-12 col-sm-6">';
                                                        echo '<div class="card card-alt">';
                                                        echo '<div class="card-main">';
                                                        echo '<div class="card-inner">';
                                                        echo '<p class="card-heading text-alt">'; echo $ime." ".$prez; echo '</p>';
                                                        echo '<p>';
                                                                    echo $post['text_post'];
                                                        echo '</p>';
                                                        echo '</div>';
                                                        echo '<div class="card-action">';
                                                        echo '<ul class="nav nav-list pull-left">';
                                                        echo '<li>';
                                                        if($uloga="Administrator"){
                                                        echo '<a href="';echo base_url();echo 'pocetna_korisnik/obrisi/'; echo $post["id_post"]; echo '"><span class="access-hide">Delete</span><span class="icon icon-delete"></span></a>';                                                            
                                                        }else{
                                                        echo '<a href="';echo base_url();echo 'pocetna_korisnik/obrisi<?php?>"><span class="access-hide">Delete</span><span class="icon icon-delete"></span></a>';                                                            
                                                        }

                                                        echo '</li>';
                                                        echo '<li class="dropdown">';
                                                        echo '<a class="dropdown-toggle" data-toggle="dropdown"><span class="access-hide">Edit</span><span class="icon icon-settings"></span></a>';
                                                        echo '<ul class="dropdown-menu">';
                                                        echo '<li>';
                                                        if($uloga="Administrator"){
                                                        echo '<a href="';echo base_url();echo 'pocetna_korisnik/izmeni/'; echo $post["id_post"]; echo '"><span class="icon icon-shuffle margin-right-half"></span>&nbsp;Izmeni</a>';
                                                        }else{
                                                        echo '<a href="';echo base_url();echo 'pocetna_korisnik/izmeni/'; echo $post["id_post"]; echo '"><span class="icon icon-shuffle margin-right-half"></span>&nbsp;Izmeni</a>';    
                                                        }
                                                        echo '</li>';
                                                        echo '</ul>';
                                                        echo '</li>';
                                                        echo '</ul>';
                                                        echo '</div>';
                                                        echo '</div>';
                                                        echo '</div>';
                                                        echo '</div>'; 
                                                    }
                                                }?>
                                                
                                            
					</div>
					<div class="col-md-3 content-fix">
						<div class="content-fix-scroll">
							<div class="content-fix-wrap">
								<div class="content-fix-inner">
                                                                    <?php echo $pagination_linkovi; ?>
                                                                    
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>