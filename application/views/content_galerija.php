<div class="content">
    <div class="content-heading">
        <div class="container container-full">
            <h1 class="heading">Galerija <small></small></h1>
        </div>
    </div>
    <div class="content-inner">
        <div class="container container-full">
            <div class="row row-fix">
                <div class="col-md-3 content-fix">
                        <div class="content-fix-scroll">
                                <div class="content-fix-wrap">
                                        <div class="content-fix-inner">


                                        </div>
                                </div>
                        </div>
                </div>
                <div class="col-md-6">
                    <fieldset>
                        <div class="form-group form-group-label">
                            <div class="row">
                                                <div class="col-lg-6 col-md-12 col-sm-8">
                                       
                                                    <?php
                                                        print form_open($form2);
                                                        print '<div class="form-group form-group-label">';
                                                        print '<div class="row">';
                                                        print '<div class="col-lg-6 col-md-12 col-sm-8">';
                                                        print form_label('Izaberite galeriju','float-select',$lbSlika);
                                                        print '<select name="opcijaGalerija" autofocus="" class="form-control" id="float-select">';
                                                        foreach ($opcijeGal as $opcije){
                                                            if($opcije['naziv_galerija']!=""){
                                                            print '<option value="'.$opcije['id_galerija'].'">'.$opcije['naziv_galerija'].'<option>';
                                                            }
                                                        }
                                                        print '<select>';
                                                        print '</div>';
                                                        print '</div>';
                                                        print '</div>';
                                                        //print form_upload($fiSlika);
                                                        print "<br/>";
                                                        print form_button($btnSlike);
                                                        print form_close();
                                                    ?>
                                                </div>
                                                <div class="col-lg-6 col-md-12 col-sm-8">
                                                  <?php
                                                    if(isset($slike_gal) && is_array($slike_gal)){
                                                    print "<br/>";
                                                    $idKor=$this->session->userdata('id_korisnik');
                                                    foreach($slike_gal as $slika){
                                                       // print "Galerija";
                                                       $attr=array('class'=>'fancybox','data-fancybox-group'=>'gallery1');
                                                       $atributi_slika=array("src"=>'mala_'.$idKor.'_'.$slika['mala_slika'],"alt"=>'galerija');
                                                        print '<div class="col-lg-6 col-md-6 col-sm-8">';
                                                        print "<div class='thumbnail'>";
                                                       // print anchor($slika['putanja_slika'],img($atributi_slika),$attr);
                                                        $putanja=base_url()."img/".$slika['putanja_slika'];
                                                            
                                                        print '<a class="fancybox" data-fancybox-group="gallery1" href="'.$putanja.'">';
                                                        print '<img  src="'.base_url()."img/mala_".$slika['mala_slika'].'" />';
                                                        print '</a>';
                                                        print '</div>';
                                                        print '</div>';
                                                    }
                                                    }else{
                                                        //print "prazna galerija";
                                                    }
                                                  ?>
                                                </div>
                            </div>
                        </div>
                    </fieldset>



                </div>
                <div class="col-md-3 content-fix">
                    <div class="content-fix-scroll">
                        <div class="content-fix-wrap">
                            <div class="content-fix-inner">

                            </div>
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </div>
            
</div>