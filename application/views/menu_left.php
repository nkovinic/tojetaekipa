<nav class="menu" id="menu">
    <div class="menu-scroll">
        <div class="menu-wrap">
            <div class="menu-content">
                <a class="menu-logo" href="<?php base_url();?>">Material</a>
                        <?php
                            if(isset($meni) && is_array($meni)){
                                $linkovi=array();
                                foreach ($meni as $link) {
                                    $linkovi[]=anchor($link['putanja_meni'],$link['naziv_meni']);
                                }
                                echo ul($linkovi,$attr);
                                echo '<hr/>';
                            }
                            if(isset($admin_meni) && is_array($admin_meni)){
                                $linkovi=array();
                                echo'<ul class="nav">';
                                echo'<li>';
                                echo"<a  href='".base_url()."admin_korisnik/clanovi'>Admin panel</a>";
                                echo'<span class="menu-collapse-toggle collapsed" data-toggle="collapse" data-target="#form-elements">';
                                echo'<i class="icon icon-close menu-collapse-toggle-close"></i>';
                                echo '<i class="icon icon-add menu-collapse-toggle-default"></i>';
                                echo'</span>';                                
                                foreach ($admin_meni as $link) {
                                    $linkovi[]=anchor($link['putanja_admin_meni'],$link['naziv_admin_meni']);
                                }
                                echo ul($linkovi,$attrA);
                                echo'<li>';                                
                                echo"</ul>";
                            }
                        ?>

            </div>
	</div>
    </div>
</nav>