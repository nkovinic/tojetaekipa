<div class="content">
    <div class="content-heading">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-lg-push-3 col-sm-10 col-sm-push-1">
                    <h1 class="heading">Registracija</h1>
                    <p>Imate nalog ulogujte se ovde <a href="<?php echo base_url();?>logovanje_registracija/login">Link</a></p>
		</div>
            </div>
	</div>
    </div>

    <div class="content-inner">
        <div class="container">
          <?php   
          $validacija=$this->session->flashdata('validacija');
          if(!empty($validacija)){  echo "<div class='alert alert-danger'>".$validacija."</div>";}
          $uspeh=$this->session->flashdata('ulogovao');
          if(!empty($uspeh))    {  echo "<div class='alert alert-success'>".$uspeh."</div>"; }?>
          <?php   echo validation_errors('<div class="alert alert-danger" role="alert">','</div>'); ?>
          <?php 
         $poruka=$this->session->flashdata('postoji');
         if(!empty($poruka)){ echo "<div class='alert alert-danger'>".$poruka."</div>";}
         $poruka2=$this->session->flashdata('uspeh');
         if(!empty($poruka2)){  echo "<div class='alert alert-success'>".$poruka2."</div>";}?>
            <?php print form_open('logovanje_registracija');?>
                <fieldset>
                    <!--<legend>Registracija</legend> -->
                     
                   <div class="form-group form-group-label">
                        <div class="row">
                            <div class="col-lg-6 col-sm-8">
                                <label class="floating-label" for="float-text">Ime</label>
                                <?php print form_input($Ime)?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-group-label">
                        <div class="row">
                            <div class="col-lg-6 col-sm-8">
                                <label class="floating-label" for="float-text">Prezime</label>
                                <?php print form_input($Prezime)?>
                            </div>
                        </div>
                    </div>
                   
                    <div class="form-group form-group-label">
                        <div class="row">
                            <div class="col-lg-6 col-sm-8">
                                <label class="floating-label" for="float-text">Email</label>
                                <?php print form_input($Email)?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-group-label">
                        <div class="row">
                            <div class="col-lg-6 col-sm-8">
                                <label class="floating-label" for="float-text">Korisnicko ime</label>
                                <?php print form_input($Username)?>
                            </div>
                        </div>
                    </div>                    
                    <div class="form-group form-group-label">
                        <div class="row">
                            <div class="col-lg-6 col-sm-8">
                                <label class="floating-label" for="float-text">Sifra</label>
                                <?php print form_input($Sifra)?>
                            </div>
                        </div>
                    </div>
                    
                    <div class="form-group form-group-label">
                        <div class="row">
                            <div class="col-lg-6 col-sm-8">
                                <label class="floating-label" for="float-text">Ponovljena sifra</label>
                                <?php print form_input($CSifra)?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group form-group-label">
                        <div class="row">
                            <div class="col-lg-6 col-sm-8">
                                <label class="floating-label" for="float-text"></label>
                                <?php print form_input($Rodjenje)?>
                            </div>
                        </div>
                    </div>                    
                </fieldset>
            <div class="form-group-btn">
                <div class="row">
                    <div class="col-lg-4 col-lg-push-2 col-md-6 col-md-push-3 col-sm-8 col-sm-push-4">
                        <?php print form_button($Registracija); ?>
                        <?php print form_button($Ponisti); ?>
                        					
                    </div>
                </div>
            </div>
            <?php print form_close();?>
        </div>
            
    </div>
</div>