<footer class="footer">
    <div class="container">
        <p>Material</p>
        
    </div>
</footer>
<div class="fbtn-container">
    <div class="fbtn-inner">
        <a class="fbtn fbtn-red fbtn-lg" data-toggle="dropdown"><span class="fbtn-text">Links</span><span class="fbtn-ori icon icon-open-in-new"></span><span class="fbtn-sub icon icon-close"></span></a>
    	<div class="fbtn-dropdown">
            <a class="fbtn" href="https://github.com/Daemonite/material" target="_blank"><span class="fbtn-text">Fork me on GitHub</span><span class="fa fa-github"></span></a>
            <a class="fbtn fbtn-blue" href="https://twitter.com/daemonites" target="_blank"><span class="fbtn-text">Follow Daemon on Twitter</span><span class="fa fa-twitter"></span></a>
            <a class="fbtn fbtn-alt" href="http://www.daemon.com.au/" target="_blank"><span class="fbtn-text">Visit Daemon Website</span><span class="fa fa-link"></span></a>
	</div>
    </div>
</div>
<script src="<?php echo base_url();?>js/jquery.min.js"></script>
<script src="<?php echo base_url();?>js/base.min.js"></script>
<script src="<?php echo base_url();?>/js/fancybox/lib/jquery-1.10.1.min.js" type="text/javascript"></script>   	
<script src="<?php echo base_url();?>/js/fancybox/source/jquery.fancybox.js" type="text/javascript"></script>
<link href="<?php echo base_url();?>/js/fancybox/source/jquery.fancybox.css" rel="stylesheet" type="text/css"/>
<script type='text/javascript'>
        $(document).ready(function(){
            var base_url="<?php echo base_url();?>";
            $("#btnPretraga").click(function(e){
                e.preventDefault();
                var unos=$("#pretraga").val();
                var zaSlanje={uneto:unos};
                $.post(base_url+'pretraga_korisnik/pretraga',zaSlanje,function(podaci){
                    var korisnici=$.parseJSON(podaci);
                    var tekst="";
                    for(var i=0;i<korisnici.length;i++){
			tekst+="<p class='card-heading text-alt'>"+korisnici[i].ime_korisnik+"<p><br/>";
                    }
                    $('#imePretraga').html(tekst);
                });
            });
           
        });

        $('.fancybox').fancybox();
    </script>
</body>
</html>