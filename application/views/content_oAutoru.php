<div class="content">
    <div class="content-heading">
        <div class="container container-full">
            <h1 class="heading">O Autoru <small></small></h1>
        </div>
    </div>
    <div class="content-inner">
        <div class="container container-full">
            <div class="row row-fix">
                <div class="col-md-3 content-fix">
                        <div class="content-fix-scroll">
                                <div class="content-fix-wrap">
                                        <div class="content-fix-inner">
                                   

                                        </div>
                                </div>
                        </div>
                </div>
                <div class="col-md-6">
                    <fieldset>
                        <div class="form-group form-group-label">
                            <div class="row">
                                <div class="col-lg-6 col-md-12 col-sm-8">
                                     <h3>Nikola Kovinić</h3>
                                    <img src="<?php print base_url();?>images/ja.jpg" height="300px" width="400px" alt="">
                                    <p>Zovem se Nikola Kovinić. Završio sam srednju elektrotehničku školu u Leskovcu, smer eletrotehničar računara. Nakon toga sam upisao Elektrotehnički fakultet (ETF), Univerzitet u Beogradu, gde sam se bavio elektronikom. Takođe sam pored ETFa upisao i visoku školu za informacione i telekomunikacione tehnologije (Visoka ICT) gde se bavim programiranjem. </p>
					<p>Aktivno studiram Visoku ICT trenutno koju i završavam. Na smeru sam internet tehnologije, podsmer WEB programiranje. Uporedo pored WEBa radim i objektno orijentsano programiranje JAVA, Arduino(mikrokontroleri) i C++.</p>
					<p>Planiram da upisem i CISCO i da se unapredim i na polju mreža.</p>
	<h3>Moja istorija</h3>
				 <div class="history-desc">
					<div class="year"><p>2006 -</p></div>
					<p class="history">Upisao srednju elektrotehničku školu u Leskovcu, smer elektrotehničar računara.</p>
				 <div class="clear"></div>
				</div>
				 <div class="history-desc">
					<div class="year"><p>2010 -</p></div>
					<p class="history">Završavam srednju i upisujem Elektrotehnički fakultet (ETF), Univerzitet u Beogradu. U 2. godini biram smer elektronika. </p>
				 <div class="clear"></div>
				</div>
				 <div class="history-desc">
					<div class="year"><p>2013 -</p></div>
					<p class="history">Upisujem visoku školu za informacione i telekomunikacione tehnologije (Visoka ICT) smer internet tehnologije, a 2014 biram podsmer WEB programiranje. </p>
				 <div class="clear"></div>
				</div>
				 <div class="history-desc">
					<div class="year"><p>2014/2015</p></div>
					<p class="history">Pored studiranja radim i na par projekta sa kolegama. 2014 sam sa kolegama radio na projekatu testiranje perifernog vida za auto školu "Pravo L", radio sam na razvoju programa tog uređaja i asistirao kolegi koji je radio na elektronici. 2015 sa istim kolegama radimo aktivno na uređaju koji će biti doniran rehabilitacionom centaru za decu obolelu od cerebralne  paralize. Takođe radimo i na projetku simulacije vožnje za autoškolu "Pravo L" </p>
				 <div class="clear"></div>
				</div>
			</div>
				<div class="col_1_of_3 span_1_of_3">
					<h3>Dalji razvoj</h3>
					<p>U planu je da sebe usmerim ka objektnom programiranju pored WEB programiranja. Tako da pored ucenja na fakultetu radim samostalno na programiranju mikrokontrolera, Arudina, Java aplikacija sa bazama podataka, kao i aplikacija za Android i Windows Phone platforme. Neki od buducih projekta:</p>
				    <div class="list">
					     <ul>
					     	<li><a href="#">Android aplikacija za autoškolu "Pravo L"</a></li>
					     	<li><a href="#">Meteorološka stanica</a></li>
					     	<li><a href="#">Windows Phone aplikacija za autoškolu "Pravo L"</a></li>
					     	<li><a href="#">Aplikacija u javi sa bazom za sindikat srpske policije-sindikalna grupa Leskovac</a></li>
					     </ul>
					 </div>
				</div>
                                        <div class="tab-pane fade" id="second-tab">
                                            <div class="row">
                                                <div class="col-lg-6 col-md-12 col-sm-8">

                                                </div>
                                                <div class="col-lg-6 col-md-12 col-sm-8">
                                                    
                                                </div>
                                            </div>
                                    </div>



                                </div>
                            </div>
                            </div>
                            </div>
                        </div>
                    </fieldset>



                </div>
                <div class="col-md-3 content-fix">
                    <div class="content-fix-scroll">
                        <div class="content-fix-wrap">
                            <div class="content-fix-inner">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
            
</div>
