<div class="avoid-fout-indicator avoid-fout-indicator-fixed">
    <div class="progress-circular progress-circular-alt progress-circular-center">
        <div class="progress-circular-wrapper">
            <div class="progress-circular-inner">
            	<div class="progress-circular-left">
                    <div class="progress-circular-spinner"></div>
                </div>
		<div class="progress-circular-gap"></div>
		<div class="progress-circular-right">
                    <div class="progress-circular-spinner"></div>
		</div>
            </div>
        </div>
    </div>                
</div>
<header class="header">
    <ul class="nav nav-list pull-left">
        <li>
            <a data-toggle="menu" href="#menu">
		<span class="access-hide">Menu</span>
		<span class="icon icon-menu icon-lg"></span>
            </a>
	</li>
    </ul>
    <a class="header-logo" href="#">Material</a>
    <ul class="nav nav-list pull-right">
        <li>
            <a data-toggle="menu" href="#profile">
                <span class="access-hide">John Smith</span>
		<span class="avatar avatar-sm"><img alt="alt text for John Smith avatar" src="<?php echo base_url();?>images/users/avatar-001.jpg"></span>
            </a>
	</li>
    </ul>
</header>