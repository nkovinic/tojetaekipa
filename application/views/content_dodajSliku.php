<div class="content">
    <div class="content-heading">
        <div class="container container-full">
            <h1 class="heading">Dodavanje slika <small></small></h1>
        </div>
    </div>
    <div class="content-inner">
        <div class="container container-full">
            <div class="row row-fix">
                <div class="col-md-3 content-fix">
                        <div class="content-fix-scroll">
                                <div class="content-fix-wrap">
                                        <div class="content-fix-inner">


                                        </div>
                                </div>
                        </div>
                </div>
                <div class="col-md-6">
                    <fieldset>
                        <div class="form-group form-group-label">
                            <div class="row">
                                <div class="col-lg-6 col-md-12 col-sm-8">
                                        <nav class="tab-nav tab-nav-green">
                                        <ul class="nav nav-justified">
                                            <li class="active">
                                                <a class="waves-color-green waves-effect" data-toggle="tab" href="#first-tab">Dodaj galeriju</a>
                                            </li>
                                            <li>
                                                <a class="waves-color-green waves-effect" data-toggle="tab" href="#second-tab">Dodaj Slike</a>
                                            </li>
                                        </ul>
                                        </nav>
                                    <div class="tab-content">
                                        <div class="tab-pane fade active in" id="first-tab">
                                                    <div class="row">
                                                        <div class="col-lg-6 col-md-12 col-sm-8">
                                                            <?php
                                                                print form_open($form1);
                                                                print form_label('Unesite naziv nove galerije','float-textarea-autosize',$lbGale1);
                                                                print form_input($tbGale1);
                                                                print "<br/>";
                                                                print form_button($btnGale);
                                                                print form_close();
                                                            ?>
                                                        </div>
                                                    </div>
                                        </div>
                                        <div class="tab-pane fade" id="second-tab">
                                            <div class="row">
                                                <div class="col-lg-6 col-md-12 col-sm-8">
                                                    <?php
                                                        print form_open_multipart($form2);
                                                        print '<div class="form-group form-group-label">';
                                                        print '<div class="row">';
                                                        print '<div class="col-lg-6 col-md-12 col-sm-8">';
                                                        print form_label('Izaberite galeriju','float-select',$lbSlika);
                                                        print '<select name="opcijaGalerija" autofocus="" class="form-control" id="float-select">';
                                                        foreach ($opcijeGal as $opcije){
                                                            if($opcije['naziv_galerija']!=""){
                                                            print '<option value="'.$opcije['id_galerija'].'">'.$opcije['naziv_galerija'].'<option>';
                                                            }
                                                        }
                                                        print '<select>';
                                                        print '</div>';
                                                        print '</div>';
                                                        print '</div>';
                                                        print form_upload($fiSlika);
                                                        print "<br/>";
                                                        print form_button($btnSlike);
                                                        print form_close();
                                                    ?>
                                                </div>
                                                <div class="col-lg-6 col-md-12 col-sm-8">
                                                  <?php 
                                                  $uspeh=$this->session->flashdata('izmenjeno');
                                                  if(!empty($uspeh))    {  echo "<div class='alert alert-success'>".$uspeh."</div>"; }
                                                  $neuspeh=$this->session->flashdata('neuspeh');
                                                  if(!empty($neuspeh))    {  echo "<div class='alert alert-danger'>".$neuspeh."</div>"; }
                                                  ?>
                                                </div>
                                            </div>
                                    </div>



                                </div>
                            </div>
                        </div>
                    </fieldset>



                </div>
                <div class="col-md-3 content-fix">
                    <div class="content-fix-scroll">
                        <div class="content-fix-wrap">
                            <div class="content-fix-inner">

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
            
</div>
