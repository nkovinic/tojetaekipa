<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of my_frontedncontroller
 *
 * @author kovin
 */
class MY_frontedncontroller extends CI_Controller {
    
    function __construct() {
        parent::__construct();
        
    }
    public function load_view($view,$vars=array()){//$view kupi view a $vars podatke    
        $korisnik=$this->session->userdata('korisnik');
        $vars['ulogovan']=$korisnik!=null;//uzimam da li je ulogovan tj njegov id
        $vars['sesije']=$this->session->all_userdata();//sesije toj id-a
        $uloga=$this->session->userdata('uloga');//ulogu

        //uzimam meni iz baze
        $this->load->model('model_meni','meni');
        $vars['attr']=array('class'=>'nav');//sredjujem atribute ul taga
        $vars['attrA']=array('class'=>'menu-collapse collapse','id'=>'form-elements');        
        $korisnik=$this->session->userdata('ime');
        $uloga=$this->session->userdata('uloga');
        if( $korisnik!="" && $uloga=='Administrator'){
            $vars['meni']=$this->meni->glavni_meni();
            $vars['admin_meni']=$this->meni->admin_meni();            
            $vars['profil_meni']=$this->meni->profil_meni();
        }else if($korisnik!="" && $uloga=='Korisnik'){
            $vars['meni']=$this->meni->glavni_meni();
            $vars['profil_meni']=$this->meni->profil_meni();            
        }

        //$vars['ul']=array('class'=>'nav navbar-nav navbar-right');
        
        if($view=='content_registar' || $view=='content_login'){
            $this->load->view('header');
            $this->load->view($view,$vars);    
            $this->load->view('footer_reg');
        }else{
            $this->load->view('header');
            $this->load->view('headerB');
            $this->load->view('menu_left',$vars);
            $this->load->view('menu_right',$vars);
            $this->load->view($view,$vars);
            $this->load->view('footer');
    }
    }
}
