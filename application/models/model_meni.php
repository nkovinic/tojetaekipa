<?php
class Model_meni extends CI_Model{
    public $naziv;
    public $idmeni;
    public $putanja;
    
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    public function glavni_meni(){
        $this->db->select('*');
        $this->db->from('meni');
        $this->db->where('aktivan_meni',1);
        return $this->db->get()->result_array();
    }
    
    public function profil_meni(){
        $this->db->select('*');
        $this->db->from('profil_meni');
        $this->db->where('aktivan_meni',1);        
        return $this->db->get()->result_array();
    }
    
    public function admin_meni(){
        $this->db->select('*');
        $this->db->from('admin_meni');
        $this->db->where('aktivan_meni',1);        
        return $this->db->get()->result_array();
    }
}
