<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of model_Sli_Gal
 *
 * @author kovin
 */
class model_Sli_Gal extends CI_Model{
    public $idKor;
    public $idGal;
    public $malaSlika;
    public $nazivGal;
    public $nazivSlike;
            
    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    public function dohvatiGalerije(){
        $this->db->select('*');
        $this->db->from('galerije');
        $this->db->where('id_korisnik',  $this->idKor);
        $this->db->where('aktivan_galerija',1);
        return $this->db->get()->result_array();        
    }
    
    public function dodajGaleriju(){
        $niz=array(
            "naziv_galerija"=>$this->nazivGal,
            "id_korisnik"=>  $this->idKor,
            "aktivan_galerija"=>1
        );
        $this->db->insert('galerije',$niz);
    }
    
    public function dodajSliku(){
        $niz=array(
            "putanja_slika"=>$this->nazivSlike,
            "mala_slika"=>  $this->malaSlika,
            "id_galerija"=>  $this->idGal,
            "id_korisnik"=>  $this->idKor,
            "aktivan_slika"=>1
        );
        $this->db->insert('slike',$niz);
    }
    public function dohvatiSlike(){
        $this->db->select('*');
        $this->db->from('slike');
        $this->db->where('id_korisnik',  $this->idKor);
        $this->db->where('aktivan_slika',1);
        $this->db->where('id_galerija',  $this->idGal);
        return $this->db->get()->result_array();        
    }
}
