<?php
class model_postovi extends CI_Model {
    public $idPost;
    public $idKorisnik;
    public $teksPost;
    public $imeKor;
    public $prezKor;


    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    
    public function dodaj_post(){
        $vreme=  time();
        $niz=array(
            "text_post"=>  $this->teksPost,
            "vreme_post"=>  $vreme,
            "id_korisnik"=>  $this->idKorisnik,
            "aktivan_post"=> 1
        );
        $this->db->insert('postovi',$niz);
        $ime=$this->imeKor." ".$this->prezKor;
        $niz1=array('naziv_log'=>"Korisnik sa imenom i prezimenom: ".$ime." je dodao post!",'id_korisnik'=>$this->idKorisnik,'vreme_log'=>$vreme);
        $this->db->insert('log',$niz1);
    }
    
    public function obrisi_post(){
        $podaci = array('aktivan_post' => 0);
        $this->db->where('id_post', $this->idPost);
        $this->db->update('postovi', $podaci);
    }
    public function obrisi_post2() {
        $this->db->where('id_post', $this->idPost);
        $this->db->delete('postovi');
    }    
    
    public function dohvati_post(){
        $this->db->select('*');
        $this->db->where('aktivan_post',1);
        $this->db->where('id_post',  $this->idPost);
        return $this->db->get('postovi')->row_array();
    }
    
    public function izmeni_post(){
        $niz=array(
            "text_post"=>  $this->teksPost
        );
        $this->db->where('id_post', $this->idPost);
        $this->db->update('postovi', $niz);
    }
    
    public function pagination_post($limit = 6){
        $offset=$this->uri->segment(3);
        $this->db->limit($limit,$offset); 
        return $this->db->get_where('postovi', array('id_korisnik' => $this->idKorisnik,'aktivan_post'=>1))->result_array();
    }
    //broj postova
    public function brojpostova(){
        return $this->db->count_all_results('postovi');
    }
       
}
?>