<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of model_korisnici
 *
 * @author Danijela
 */
class Model_korisnici extends CI_Model{
    public $idkorisnik;
    public $ime;
    public $prezime;
    public $email;
    public $user;
    public $password;    
    public $datumRodjenja;
    public $uloga;
    public $aktivan;
            
    function __construct() {
        parent::__construct();
        $this->load->database();
    }
    //za logovanje
    function dohvati(){
            $uslov=array("mail_korisnik"=>$this->email,"pass_korisnik"=>$this->password);
            $this->db->select('*');
            $this->db->from('korisnici');
            $this->db->join('uloga','uloga.id_uloga=korisnici.id_uloga');
            $this->db->where($uslov);
            $query=$this->db->get(); //VAZNOOOO
            return $query->row_array();
    }
    
    function kontakt(){
        $this->db->select('*');
        $this->db->from('korisnici');
        $this->db->where('id_korisnik',$this->idkorisnik);
        return $this->db->get()->row_array();
    }
    //za proveru da li vec postoji takav korisnik
    function registracija(){
        $this->db->select('*');
        $this->db->from('korisnici');
        $this->db->where('mail_korisnik',$this->email);
        return $this->db->get()->num_rows();
    }
    //za registraciju
    function unesi(){
        $niz=array("ime_korisnik"=>$this->ime,"prezime_korisnik"=>$this->prezime,
                   "username_korisnik"=>$this->user,"mail_korisnik"=>$this->email,
                   "pass_korisnik"=>md5($this->password),"id_uloga"=>$this->uloga,
                   "datum_rodjenja"=>$this->datumRodjenja,"aktivan_korisnik"=>$this->aktivan);
        
        $this->db->insert('korisnici',$niz);
//        if(!empty($this->uloge)){
//            $id=$this->db->insert_id();
//            foreach ($this->uloge as $uloga){
//                $unos=array('idkorisnik'=>$id,'iduloga'=>$uloga);
//                $this->db->insert('korisnik_uloga',$unos);
//            }
        $id=$this->db->insert_id();
        $vreme=time();
        $this->idkorisnik=$id;
        $rez=$this->korisnik();
        $niz1=array('naziv_log'=>"Korisnik sa e-mail adresom :".$rez['mail_korisnik']." se registrovao!",'id_korisnik'=>$this->idkorisnik,'vreme_log'=>$vreme);
        $this->db->insert('log',$niz1);
    }
    //za izvlacenje emaila i id-a
    function korisnik(){
        $this->db->select('*');
        $this->db->from('korisnici');
        $this->db->where('id_korisnik',$this->idkorisnik);
        return $this->db->get()->row_array();
    }
    function obrisiKorisnika(){
        $podaci = array('aktivan_korisnik' => 0);
        $this->db->where('id_korisnik', $this->idkorisnik);
        $this->db->update('korisnici', $podaci);;
    }
    function izmeniIme(){
        $podaci = array('ime_korisnik' => $this->ime);
        $this->db->where('id_korisnik', $this->idkorisnik);
        $this->db->update('korisnici', $podaci);
    }
    public function prikazi(){
        $naziv=$this->input->post('uneto');
        //if(!empty($naziv)){
            $this->db->select('ime_korisnik');
        
            $this->db->like('ime_korisnik',$naziv);
        
        return $this->db->get('korisnici')->result_array();
    }
}