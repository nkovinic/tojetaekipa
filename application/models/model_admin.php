<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of model_admin
 *
 * @author kovin
 */
class model_admin extends CI_Model {
    public $idKor;
    public $imeKor;
    public $prezKor;
    public $aktivanK;
    public $idGal;
    public $idLog;
    public function __construct() {
        parent::__construct();
        $this->load->database();
    }
    public function dohvatiKorisnike(){
        $this->db->select('*');
        $this->db->from('korisnici');
        $this->db->where('aktivan_korisnik',1);
        return $this->db->get()->result_array();
    }    
    public function obrisiGaleriju(){
        $podaci = array('aktivan_galerija' => 0);
        $this->db->where('id_galerija', $this->idGal);
        $this->db->update('galerije', $podaci);;
    }
    public function dohvatiGalerije(){
        $this->db->select('*');
        $this->db->from('galerije');
        $this->db->join('korisnici','galerije.id_korisnik=korisnici.id_korisnik');
        $this->db->where('aktivan_galerija',1);
        return $this->db->get()->result_array();
    }
    public function obrisiLog(){
        $this->db->where('id_log', $this->idLog);
        $this->db->delete('log');;
    }
    public function dohvatiLog(){
        $this->db->select('*');
        $this->db->from('log');
        $this->db->join('korisnici','log.id_korisnik=korisnici.id_korisnik');
        $this->db->where('aktivan_korisnik',1);
        return $this->db->get()->result_array();
    }    
}
