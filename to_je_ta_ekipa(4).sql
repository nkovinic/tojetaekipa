-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 17, 2016 at 10:18 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `to_je_ta_ekipa`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_meni`
--

CREATE TABLE `admin_meni` (
  `id_admin_meni` int(5) NOT NULL,
  `naziv_admin_meni` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `putanja_admin_meni` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `aktivan_meni` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `admin_meni`
--

INSERT INTO `admin_meni` (`id_admin_meni`, `naziv_admin_meni`, `putanja_admin_meni`, `aktivan_meni`) VALUES
(1, 'Članovi', 'admin_korisnik/clanovi', 1),
(2, 'Galerije', 'admin_korisnik/galerije', 1),
(3, 'Log', 'admin_korisnik/log', 1);

-- --------------------------------------------------------

--
-- Table structure for table `galerije`
--

CREATE TABLE `galerije` (
  `id_galerija` int(10) NOT NULL,
  `naziv_galerija` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `id_korisnik` int(10) NOT NULL,
  `aktivan_galerija` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `galerije`
--

INSERT INTO `galerije` (`id_galerija`, `naziv_galerija`, `id_korisnik`, `aktivan_galerija`) VALUES
(1, 'Roma', 1, 1),
(2, 'FORZA ROMA', 1, 1),
(5, 'Blizanci', 9, 1);

-- --------------------------------------------------------

--
-- Table structure for table `korisnici`
--

CREATE TABLE `korisnici` (
  `id_korisnik` int(10) NOT NULL,
  `ime_korisnik` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `prezime_korisnik` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `username_korisnik` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `mail_korisnik` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `pass_korisnik` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_uloga` int(1) NOT NULL,
  `datum_rodjenja` date NOT NULL,
  `aktivan_korisnik` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `korisnici`
--

INSERT INTO `korisnici` (`id_korisnik`, `ime_korisnik`, `prezime_korisnik`, `username_korisnik`, `mail_korisnik`, `pass_korisnik`, `id_uloga`, `datum_rodjenja`, `aktivan_korisnik`) VALUES
(1, 'Nikola', 'Kovinic', 'komaislaw', 'komaislaw@gmail.com', '809c84935865512401c383f1f18b86d6', 2, '1991-03-07', 1),
(2, 'Marko', 'Spasojević', 'Marko', 'marko.m.spasojevic@ict.edu.rs', 'f5b861b8a0ce052d0a5804cb0e241ac9', 2, '2016-03-01', 1),
(9, 'Vanja', 'Kovinic', 'vanjakovinic', 'vanjakovinic@gmail.com', 'ff3545fdee237ea6e7aa9e15e4c4da6f', 1, '0000-00-00', 1);

-- --------------------------------------------------------

--
-- Table structure for table `log`
--

CREATE TABLE `log` (
  `id_log` int(10) NOT NULL,
  `naziv_log` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `id_korisnik` int(10) NOT NULL,
  `vreme_log` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `log`
--

INSERT INTO `log` (`id_log`, `naziv_log`, `id_korisnik`, `vreme_log`) VALUES
(85, 'Korisnik sa imenom i prezimenom: Nikola Kovinic je dodao post!', 1, 1458045356),
(86, 'Korisnik sa imenom i prezimenom: Nikola Kovinic je dodao post!', 1, 1458045603),
(87, 'Korisnik sa imenom i prezimenom: Nikola Kovinic je dodao post!', 1, 1458045693),
(88, 'Korisnik sa imenom i prezimenom: Nikola Kovinic je dodao post!', 1, 1458045874),
(89, 'Korisnik sa imenom i prezimenom: Nikola Kovinic je dodao post!', 1, 1458046154);

-- --------------------------------------------------------

--
-- Table structure for table `meni`
--

CREATE TABLE `meni` (
  `id_meni` int(5) NOT NULL,
  `naziv_meni` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `putanja_meni` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `aktivan_meni` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `meni`
--

INSERT INTO `meni` (`id_meni`, `naziv_meni`, `putanja_meni`, `aktivan_meni`) VALUES
(1, 'Postovi', 'pocetna_korisnik/', 1),
(2, 'Galerija', 'galerija_korisnik', 1),
(3, 'Dodaj sliku', 'slike_korisnik/', 1),
(4, 'Pretraga', 'pretraga_korisnik/', 1),
(5, 'O Autoru', 'OAutoru/', 1),
(6, 'Dokumentacija', 'pocetna_korisnik/download', 0),
(7, 'Dokumentacija', 'pocetna_korisnik/download', 1);

-- --------------------------------------------------------

--
-- Table structure for table `postovi`
--

CREATE TABLE `postovi` (
  `id_post` int(10) NOT NULL,
  `text_post` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `vreme_post` int(20) NOT NULL,
  `id_korisnik` int(10) NOT NULL,
  `aktivan_post` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `postovi`
--

INSERT INTO `postovi` (`id_post`, `text_post`, `vreme_post`, `id_korisnik`, `aktivan_post`) VALUES
(71, 'Cao Vanja. :) <3', 1458022646, 9, 1),
(74, 'Ovde ide neki post. Izmenjen.2', 1458044666, 1, 1),
(75, 'Vanja', 1458044701, 9, 1),
(76, 'Korisnik2', 1458044831, 1, 1),
(77, 'Vanja <3 ', 1458044852, 9, 0),
(78, 'Korisnik3', 1458045356, 1, 1),
(79, 'Korisnik3', 1458045603, 1, 0),
(80, 'Korisnik3', 1458045693, 1, 1),
(81, 'Korisnik3', 1458045874, 1, 1),
(82, 'Korisnik47', 1458046154, 1, 1),
(83, 'Korisnik2', 1458046165, 1, 1),
(84, 'Vanja Kovinic :P', 1458046364, 9, 1),
(85, 'Ovde ide neki post. Izmenjen.', 1458048228, 1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `profil_meni`
--

CREATE TABLE `profil_meni` (
  `id_profil_meni` int(5) NOT NULL,
  `naziv_profil_meni` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `putanja_profil_meni` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `aktivan_meni` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `profil_meni`
--

INSERT INTO `profil_meni` (`id_profil_meni`, `naziv_profil_meni`, `putanja_profil_meni`, `aktivan_meni`) VALUES
(2, 'Odjavi se', 'logovanje_registracija/logout', 1);

-- --------------------------------------------------------

--
-- Table structure for table `slike`
--

CREATE TABLE `slike` (
  `id_slika` int(10) NOT NULL,
  `putanja_slika` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `mala_slika` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `id_galerija` int(10) NOT NULL,
  `id_korisnik` int(10) NOT NULL,
  `aktivan_slika` int(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `slike`
--

INSERT INTO `slike` (`id_slika`, `putanja_slika`, `mala_slika`, `id_galerija`, `id_korisnik`, `aktivan_slika`) VALUES
(8, '1_1458241194.jpg', '1_1458241194.jpg', 1, 1, 1),
(9, '1_1458241223.jpg', '1_1458241223.jpg', 2, 1, 1),
(10, '9_1458241285.jpg', '9_1458241285.jpg', 5, 9, 1),
(11, '9_1458241323.jpg', '9_1458241323.jpg', 5, 9, 1);

-- --------------------------------------------------------

--
-- Table structure for table `uloga`
--

CREATE TABLE `uloga` (
  `id_uloga` int(5) NOT NULL,
  `naziv_uloga` varchar(15) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `uloga`
--

INSERT INTO `uloga` (`id_uloga`, `naziv_uloga`) VALUES
(1, 'Korisnik'),
(2, 'Administrator');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_meni`
--
ALTER TABLE `admin_meni`
  ADD PRIMARY KEY (`id_admin_meni`);

--
-- Indexes for table `galerije`
--
ALTER TABLE `galerije`
  ADD PRIMARY KEY (`id_galerija`);

--
-- Indexes for table `korisnici`
--
ALTER TABLE `korisnici`
  ADD PRIMARY KEY (`id_korisnik`),
  ADD UNIQUE KEY `mail_korisnik` (`mail_korisnik`);

--
-- Indexes for table `log`
--
ALTER TABLE `log`
  ADD PRIMARY KEY (`id_log`);

--
-- Indexes for table `meni`
--
ALTER TABLE `meni`
  ADD PRIMARY KEY (`id_meni`);

--
-- Indexes for table `postovi`
--
ALTER TABLE `postovi`
  ADD PRIMARY KEY (`id_post`);

--
-- Indexes for table `profil_meni`
--
ALTER TABLE `profil_meni`
  ADD PRIMARY KEY (`id_profil_meni`);

--
-- Indexes for table `slike`
--
ALTER TABLE `slike`
  ADD PRIMARY KEY (`id_slika`);

--
-- Indexes for table `uloga`
--
ALTER TABLE `uloga`
  ADD PRIMARY KEY (`id_uloga`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_meni`
--
ALTER TABLE `admin_meni`
  MODIFY `id_admin_meni` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `galerije`
--
ALTER TABLE `galerije`
  MODIFY `id_galerija` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `korisnici`
--
ALTER TABLE `korisnici`
  MODIFY `id_korisnik` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `log`
--
ALTER TABLE `log`
  MODIFY `id_log` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=91;
--
-- AUTO_INCREMENT for table `meni`
--
ALTER TABLE `meni`
  MODIFY `id_meni` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `postovi`
--
ALTER TABLE `postovi`
  MODIFY `id_post` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=86;
--
-- AUTO_INCREMENT for table `profil_meni`
--
ALTER TABLE `profil_meni`
  MODIFY `id_profil_meni` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `slike`
--
ALTER TABLE `slike`
  MODIFY `id_slika` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `uloga`
--
ALTER TABLE `uloga`
  MODIFY `id_uloga` int(5) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
